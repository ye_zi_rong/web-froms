﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebForms
{
    public partial class Login : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }


        protected void Lg_Authenticate(object sender, AuthenticateEventArgs e)
        {
            var username = Lg.UserName.Trim();
            var password = Lg.Password.Trim();

            var sql = string.Format("select * from Users where Username='{0}' and Password='{1}'", username, password);
            var dt = DbHelper.GetDatatable(sql);

            if (dt.Rows.Count == 1)
            {
                //验证通过，允许跳转
                var row = dt.Rows[0];
                var IsActived = (bool)row.ItemArray[3];
                var IsDeleted = (bool)row.ItemArray[4];
                if (IsDeleted)
                {
                    //账号已删除
                    lblComment.Text = "暂无此账号，请检查用户名是否正确！如有疑问请致电客服，客服电话—10086";
                }
                else
                {
                    //账号未删除继续判断用户状态
                    if (IsActived)
                    {
                        //已启用，允许跳转用户首页
                        Response.Redirect("AfterLogin.aspx");
                        lblComment.Text = "用户名为:" + username + "的用户登陆成功！欢迎来到地球!";
                    }
                    else
                    {
                        //账号被停用
                        lblComment.Text = "当前账号因为使用外挂或脚本，已被封停9999天，请爱惜自己的账号，裁决之镰绝不姑息任何使用外挂的玩家！";
                    }
                }
            }
            else
            {
                //验证失败，请重试
                lblComment.Text = "账号或密码错误，请确认后重试！";
            }
        }
    }
    }

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebForms
{
    public partial class AfterLogin : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                FillData();
            }
        }
        public void FillData()
        {
            var sql = string.Format("select Id,Username 用户名,Password 密码,IsActived 是否启用,IsDeleted 是否删除 from Users");
            var dt = DbHelper.GetDatatable(sql);
            Gv1.DataSource = dt;
            Gv1.DataBind();
        }
        protected void Gv1_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            Gv1.EditIndex = -1;
            FillData();
        }

        protected void Gv1_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            var Id = GetCellValue(e.RowIndex, 1);
            var sql = string.Format("delete Users where Id='{0}'", Id);

            DbHelper.AddOrDeleteOrUpdate(sql);
            Gv1.EditIndex = -1;
            FillData();
        }

        protected void Gv1_RowEditing(object sender, GridViewEditEventArgs e)
        {
            Gv1.EditIndex = e.NewEditIndex;
            FillData();
        }

        protected void Gv1_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            var Id = GetCellValue(e.RowIndex, 0);
            var username = GetCellValue(e.RowIndex, 1);
            var password = GetCellValue(e.RowIndex, 2);
            var IsActived = GetCellValue(e.RowIndex, 3);
            var IsDeleted = GetCellValue(e.RowIndex, 4);

            var sql = string.Format("Update Users set Username='{0}',Password='{1}',IsActived='{2}',IsDeleted='{3}' where Id='{4}'", username, password, IsActived, IsDeleted, Id);
            DbHelper.AddOrDeleteOrUpdate(sql);
            Gv1.EditIndex = -1;
            FillData();
            
            
        }

        protected void Add_Click(object sender, EventArgs e)
        {
            string username = this.username.Text.ToString();
            string password = this.password.Text.ToString();
            string remarks = this.Remarks2.Text.ToString();

            var sql = string.Format("insert into Users(Username,Password,Remarks) values('{0}','{1}','{2}')", username, password, remarks);
            DbHelper.AddOrDeleteOrUpdate(sql);
            Gv1.EditIndex = -1;
            FillData();
        }
        private string GetCellValue(int rowIndex,int colIndex)
        {
            var control = Gv1.Rows[rowIndex].Cells[colIndex];
            var res = control.Controls.Count > 0 ? ((TextBox)control.Controls[0]).Text : control.Text;
            return res;
        }


    }
}
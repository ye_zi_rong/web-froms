﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;

namespace WebForms
{
    public class DbHelper
    {
        private static readonly string conString = "server=.;database=Admin3000;uid=sa;pwd=123456";

        public static DataTable GetDatatable(string sql)
        {
            SqlDataAdapter adapter = new SqlDataAdapter(sql, conString);
            DataTable table = new DataTable();
            adapter.Fill(table);
            return table;
        }

        public static void AddOrDeleteOrUpdate(string sql)
        {
            SqlConnection connection = new SqlConnection(conString);
            SqlCommand command = new SqlCommand(sql, connection);
            connection.Open();
            command.ExecuteNonQuery();
            connection.Close();
        }
    }
}
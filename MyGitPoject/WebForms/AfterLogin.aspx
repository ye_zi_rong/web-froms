﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AfterLogin.aspx.cs" Inherits="WebForms.AfterLogin" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>我的网页</title>
    <script>
        alert("登录成功");
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <asp:GridView ID="Gv1" runat="server" OnRowCancelingEdit="Gv1_RowCancelingEdit" OnRowDeleting="Gv1_RowDeleting"
                 OnRowEditing="Gv1_RowEditing" OnRowUpdating="Gv1_RowUpdating" BackColor="White" BorderColor="#DEDFDE" BorderStyle="None" BorderWidth="1px" CellPadding="4" ForeColor="Black" GridLines="Vertical" Width="488px">
                <AlternatingRowStyle BackColor="White" />
                <Columns>
                    <asp:CommandField ButtonType="Button" ShowDeleteButton="True" ShowEditButton="True" />
                </Columns>
                <FooterStyle BackColor="#CCCC99" />
                <HeaderStyle BackColor="#6B696B" Font-Bold="True" ForeColor="White" />
                <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" />
                <RowStyle BackColor="#F7F7DE" />
                <SelectedRowStyle BackColor="#CE5D5A" Font-Bold="True" ForeColor="White" />
                <SortedAscendingCellStyle BackColor="#FBFBF2" />
                <SortedAscendingHeaderStyle BackColor="#848384" />
                <SortedDescendingCellStyle BackColor="#EAEAD3" />
                <SortedDescendingHeaderStyle BackColor="#575357" />

            </asp:GridView>
        </div>
        <table>
            <tr>
                <td>
                    <asp:Label ID="uid" runat="server" Text="账户："></asp:Label>        
                    <asp:TextBox ID="username" runat="server"></asp:TextBox>
                </td>
            </tr>

            <tr>
                <td>
                    <asp:Label ID="pwd" runat="server" Text="密码："></asp:Label>        
                    <asp:TextBox ID="password" runat="server"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="Remarks" runat="server" Text="备注："></asp:Label>        
                    <asp:TextBox ID="Remarks2" runat="server"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Button ID="Add" runat="server" Text="确认添加" OnClick="Add_Click" />
                </td>
            </tr>
        </table>
    </form>
</body>
</html>

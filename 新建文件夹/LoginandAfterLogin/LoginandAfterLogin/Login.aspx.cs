﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace LoginandAfterLogin
{
    public partial class Login : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void Lg_Authenticate(object sender, AuthenticateEventArgs e)
        {
            var Username = Lg.UserName.Trim();
            var Password = Lg.Password.Trim();

            var sql = string.Format("select * from Users where Username='{0}' and Password='{1}'", Username, Password);
            var dt = DbHelper.GetDataTable(sql);

            if (dt.Rows.Count == 1)
            {
                //验证成功
                var row = dt.Rows[0];
                var IsActived = (bool)row.ItemArray[3];
                var IsDeleted = (bool)row.ItemArray[4];
                if (IsDeleted)
                {
                    //账号已被删除
                    lblcomment.Text = "该账号已被删除";
                }
                else
                {
                    //账号已启用，跳转到用户界面
                    if (IsActived)
                    {
                        Response.Redirect("AfterLogin.aspx");
                    }
                }
            }
            else
            {
                lblcomment.Text = "账号或密码错误，请重试！";
            }
        }

        protected void Zc_Click(object sender, EventArgs e)
        {
            Response.Redirect("zhuce.aspx");
        }
    }
}
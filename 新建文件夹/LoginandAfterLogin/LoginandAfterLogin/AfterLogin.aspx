﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AfterLogin.aspx.cs" Inherits="LoginandAfterLogin.AfterLogin" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <asp:GridView ID="Gv1" runat="server" BackColor="White" BorderColor="White" BorderWidth="2px" CellPadding="3" GridLines="None" Width="1099px"
                OnRowCancelingEdit="Gv1_RowCancelingEdit" OnRowDeleting="Gv1_RowDeleting" OnRowEditing="Gv1_RowEditing" OnRowUpdating="Gv1_RowUpdating" BorderStyle="Ridge" CellSpacing="1" >

                <Columns>
                    <asp:CommandField ButtonType="Button" HeaderText="操作类型" ShowDeleteButton="True" ShowEditButton="True" ShowHeader="True" />
                </Columns>

                <FooterStyle BackColor="#C6C3C6" ForeColor="Black" />
                <HeaderStyle BackColor="#4A3C8C" Font-Bold="True" ForeColor="#E7E7FF" />
                <PagerStyle BackColor="#C6C3C6" ForeColor="Black" HorizontalAlign="Right" />
                <RowStyle BackColor="#DEDFDE" ForeColor="Black" />
                <SelectedRowStyle BackColor="#9471DE" ForeColor="White" Font-Bold="True" />
                <SortedAscendingCellStyle BackColor="#F1F1F1" />
                <SortedAscendingHeaderStyle BackColor="#594B9C" />
                <SortedDescendingCellStyle BackColor="#CAC9C9" />
                <SortedDescendingHeaderStyle BackColor="#33276A" />

            </asp:GridView>
        </div>
        <div style="margin-left : 5px">
            <asp:Button ID="btnAdd" runat="server" Text="添加" OnClick="btnAdd_Click" />
        </div>
        
        
    </form>
</body>
</html>

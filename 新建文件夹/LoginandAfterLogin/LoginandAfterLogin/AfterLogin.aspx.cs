﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace LoginandAfterLogin
{
    public partial class AfterLogin : System.Web.UI.Page
    {
        private DataTable Data
        {
            get
            {
                var sql = "select Id,ProductName,ShortDesc,Remarks from Products";
                var dt = DbHelper.GetDataTable(sql);
                return dt;
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                FillData();
            }
        }
        public void FillData()
        {
            Gv1.DataSource = this.Data;
            Gv1.DataBind();
        }

        protected void Gv1_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e) //编辑
        {
            Gv1.EditIndex = -1;
            FillData();
        }

        protected void Gv1_RowDeleting(object sender, GridViewDeleteEventArgs e)        //删除
        {
            var Id = GetCellValue(e.RowIndex, 1);
            var sql = string.Format("delete Products where Id={0}", Id);

            DbHelper.Exe(sql);
            Gv1.EditIndex = -1;
            FillData();
        }

        protected void Gv1_RowEditing(object sender, GridViewEditEventArgs e)       //取消
        {
            Gv1.EditIndex = e.NewEditIndex;
            FillData();
        }

        protected void Gv1_RowUpdating(object sender, GridViewUpdateEventArgs e)    //更新
        {
            var IdString = GetCellValue(e.RowIndex, 1);
            var IsOk = int.TryParse(IdString, out int Id);

            var ProductName = GetCellValue(e.RowIndex,2);
            var ShortDesc = GetCellValue(e.RowIndex, 3);
            var Remarks = GetCellValue(e.RowIndex, 4);

            if (!IsOk)
            {
                var sql = string.Format("Update Products set ProductName='{0}',ShortDesc='{1}',Remarks='{2}' where Id={3}", ProductName, ShortDesc, Remarks, Id);
                DbHelper.Exe(sql);
                Gv1.EditIndex = -1;
                FillData();
            }
            else
            {
                var sql = string.Format("insert into Products(ProductName,ShortDesc,Remarks) values('{0}','{1}','{2}')", ProductName, ShortDesc, Remarks);
                DbHelper.Exe(sql);
                Gv1.EditIndex = -1;
                FillData();
            }
            
        }

        private string GetCellValue(int rowIndex,int colIndex)
        {
            var control = Gv1.Rows[rowIndex].Cells[colIndex];
            var result = control.Controls.Count > 0 ? ((TextBox)control.Controls[0]).Text : control.Text;
            return result;
        }

        protected void btnAdd_Click(object sender, EventArgs e)
        {
            var tempDt = this.Data;
            var dt = tempDt.Copy();

            dt.Rows.Add(dt.NewRow());

            Gv1.DataSource = dt;
            Gv1.DataBind();

            Gv1.EditIndex = dt.Rows.Count - 1;

            Gv1.DataSource = dt;
            Gv1.DataBind();
        }
    }
}
create database Admin3001
go
use Admin3001

go

create table Users
(
	Id int primary key identity ,
	Username nvarchar(100) not null,
	Password nvarchar(100) not null,
	IsActived bit default 1 not null,
	IsDeleted bit default 0 not null,
	CreatedTime datetime default getdate(),
	UpdatedTime datetime default getdate(),
	Remarks nvarchar(800)
)
insert into Users(Username,Password) values('衷','20011121'),('Eliauk','20020704')

create table Brands
(
	Id int primary key identity,
	BrandName nvarchar(100) not null,
	IsActived bit default 1 not null,
	IsDeleted bit default 0 not null,
	CreatedTime datetime default getdate(),
	UpdatedTime datetime default getdate(),
	Remarks nvarchar(800)
)
insert into Brands(BrandName) values('苹果'),('三星'),('Vivo'),('华为')


create table Products
(
	Id int primary key identity,
	BrandId int foreign key(BrandId) references Brands(Id),
	ProductName nvarchar(80) not null,
	ShortDesc nvarchar(80) not null,
	IsActived bit default 1 not null,
	IsDeleted bit default 0 not null,
	CreatedTime datetime default getdate(),
	UpdatedTime datetime default getdate(),
	Remarks nvarchar(800)
)
insert into Products(BrandId,ProductName,ShortDesc) values(1,'Iphone12 Pro','6.1英寸 苹果 A14 1200万像素超广角镜头+1200万像素广角镜头')

insert into Products(BrandId,ProductName,ShortDesc) values(2,'三星GALAXY Note 10','6.3英寸2280x1080像素 相机：前置1000万后置1200万广角镜头+1200万像素长焦镜头+1600万像素超广角镜头 ')
insert into Products(BrandId,ProductName,ShortDesc) values(3,'VivoIQOOneo 855版','6.3英寸2280x1080像素 相机：前置1000万后置1200万广角镜头+1200万像素长焦镜头+1600万像素超广角镜头 ')
insert into Products(BrandId,ProductName,ShortDesc) values(4,'联想小新潮1200xl','办公笔记本方便，灵巧 ')

select * from Users
select * from Brands
select * from Products


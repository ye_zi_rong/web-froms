﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="WebApplication1.Login" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
    <script>
        alert("这是一个登录系统，点击确定开始登陆。")
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <asp:Login ID="Lg" runat="server" OnAuthenticate="Lg_Authenticate" >

            </asp:Login>
        </div>
    </form>
    <div>
        <asp:Label ID="lblcomment" runat="server" Text="" ForeColor="Red">

        </asp:Label>
    </div>
</body>
</html>

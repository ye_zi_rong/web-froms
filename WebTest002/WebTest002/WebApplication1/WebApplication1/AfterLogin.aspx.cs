﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebApplication1
{
    public partial class AfterLogin : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                FillData();
            }
        }
        public void FillData()
        {
            var sql = "select * from Products";
            var dt = DbHelper.GetDataTable(sql);

            Gv1.DataSource = dt;
            Gv1.DataBind();
        }
        protected void Gv1_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e) //取消编辑
        {
            Gv1.EditIndex = -1;
            FillData();
        }

        protected void Gv1_RowDeleting(object sender, GridViewDeleteEventArgs e) //删除
        {
            var Id = GetCellValue(e.RowIndex, 1);
            var sql = string.Format("delete from Products where Id = {0}", Id);
            DbHelper.Exe(sql);

            Gv1.EditIndex = -1;
            FillData();


        }

        protected void Gv1_RowEditing(object sender, GridViewEditEventArgs e)   //编辑
        {
            Gv1.EditIndex = e.NewEditIndex;
            FillData();
        }

        protected void Gv1_RowUpdating(object sender, GridViewUpdateEventArgs e)    //更新
        {
            var Id = GetCellValue(e.RowIndex, 1);
            var ProductName = GetCellValue(e.RowIndex, 2);
            var ShortDesc = GetCellValue(e.RowIndex, 3);

            var sql = string.Format("Update Products set ProdectName='{0}',ShortDesc='{1}' where Id ={2}", ProductName, ShortDesc, Id);
            DbHelper.Exe(sql);

            Gv1.EditIndex = -1;
            FillData();
        }

        private string GetCellValue(int rowIndex, int colIdex)
        {
            var control = Gv1.Rows[rowIndex].Cells[colIdex];
            var res = control.Controls.Count > 0 ? ((TextBox)control.Controls[0]).Text : control.Text;
            return res;
        }

        protected void Add_Click(object sender, EventArgs e)    //添加
        {
            
            string ProductName = this.ProductName.Text;
            string ShortDesc = this.ShortDesc.Text;
            string BrandName = this.BrandName.Text;

            var sql = string.Format("insert into Products(ProdectName,ShortDesc,BrandId) values('{0}','{1}')",ProductName, ShortDesc);
            DbHelper.Exe(sql);

            Gv1.EditIndex = -1;
            FillData();
            var sql2 = string.Format("insert into Brands(BrandName) values('{0}')", BrandName);
            DbHelper.Exe(sql2);
            Gv1.EditIndex = -1;
            FillData();


        }
    }    
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebApplication1
{
    public partial class Login : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void Lg_Authenticate(object sender, AuthenticateEventArgs e)
        {
            var username = Lg.UserName.Trim();
            var password = Lg.Password.Trim();

            var sql = string.Format("select * from Users where Username='{0}' and Password = '{1}'", username, password);
            var dt = DbHelper.GetDataTable(sql);

            if(dt.Rows.Count == 1)
            {
                //验证通过,跳转页面                               
                var row = dt.Rows[0];
                var IsActived = (bool)row.ItemArray[3];
                var IsDeleted = (bool)row.ItemArray[4];

                if (IsDeleted)
                {
                    //账号删除
                    lblcomment.Text = "暂无此账号，如有疑问请拨打客服电话—10086";
                }
                else
                {
                    if (IsActived)
                    {
                        //已启用，允许跳转
                        Response.Redirect("AfterLogin.aspx");
                    }
                }
            }
            else
            {
                //验证失败，不允许跳转
                lblcomment.Text = "账户或密码错误，请重试！";
                Response.Redirect("Login.aspx");
            }
        }
    }
}
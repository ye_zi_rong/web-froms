create database Admin3000
go

use Admin3000

go

create table Users
(
	Id int primary key identity,
	Username nvarchar(80) not null,
	Password nvarchar(80) not null,
	IsActived bit default 1 not null,
	IsDeleted bit default 0 not null,
	CreatedTime datetime default getdate(),
	UpdatedTime datetime default getdate(),
	Remarks nvarchar(800)
)

insert into Users(Username,Password) values('Eliauk','20020704'),('衷','20011121')


create table Products
(
	Id int primary key identity,
	ProdectName nvarchar(80) not null,
	ShortDesc nvarchar(80) null,
	BrandId int foreign key(BrandId) references Brands(Id),
	IsActived bit default 1 not null,
	IsDeleted bit default 0 not null,
	CreatedTime datetime default getdate(),
	UpdatedTime datetime default getdate(),
	Remarks nvarchar(800)
)

insert into Products(ProdectName,ShortDesc,BrandId) values ('Iphone12Pro','谁用谁知道',1),('联想小新潮','xxxx',2),('三星note40','firethehong',3)

create table Brands
(
	Id int primary key identity,
	BrandName nvarchar(80),
	IsActived bit default 1 not null,
	IsDeleted bit default 0 not null,
	CreatedTime datetime default getdate(),
	UpdatedTime datetime default getdate(),
	Remarks nvarchar(800)
)

insert into Brands(BrandName) values('苹果'),('联想'),('三星')

select * from Users
select * from Products
select * from Brands